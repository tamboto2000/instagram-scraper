FROM ubuntu:latest

LABEL maintainer="Franklin Collin Tamboto <tamboto2000@gmail.com>"

WORKDIR /app

COPY app/app ./

RUN apt-get update
RUN apt-get install -y ca-certificates

EXPOSE 8000

CMD ["./app"]