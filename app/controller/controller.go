package controller

import (
	"controller"
)

var ctrl = controller.NewController()

//Controllers return controller collection, register controllers in this function
func Controllers() *controller.Controller {
	ctrl.Add("create_session", createIGSession)
	ctrl.Add("check_session", checkOldInstagramSession)
	ctrl.Add("start_scraper", startScraper)
	ctrl.Add("download", download)

	return ctrl
}
