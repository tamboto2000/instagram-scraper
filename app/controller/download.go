package controller

import (
	"bytes"
	"compress/gzip"
	"controller"
	"encoding/base64"
	"instagram_scraper/app/models"
	"io/ioutil"

	"github.com/jinzhu/gorm"
)

func download(ctx *controller.Context) {
	userID := ctx.Request.URL.Query().Get("userID")
	reqID := ctx.Request.URL.Query().Get("requestID")
	ty := ctx.Request.URL.Query().Get("type")

	if userID == "" || reqID == "" || ty == "" {
		ctx.BadRequest("userID, type, and requestID need to be filled")
		return
	}

	if ty != "json" && ty != "csv" {
		ctx.BadRequest("unrecognized value for parameter 'type'. Recognized value are: 'json', 'csv'")
		return
	}

	if ty == "csv" {
		data, err := loadCSV(ctx.App.DB, reqID, userID)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not ready yet or you have bad credential")
				return
			}

			ctx.InternalError(err.Error())
			return
		}

		ctx.ServeContent("result.zip", data)
		return
	} else if ty == "json" {
		data, err := loadJSON(ctx.App.DB, reqID, userID)
		if err != nil {
			if err.Error() == gorm.ErrRecordNotFound.Error() {
				ctx.NotFound("data might be not ready yet or you have bad credential")
				return
			}

			ctx.InternalError(err.Error())
			return
		}

		ctx.ServeContent("result.json", data)
	}
}

func loadJSON(db *gorm.DB, reqID, userID string) ([]byte, error) {
	igResult := new(models.InstagramResult)
	if err := db.Take(igResult, models.InstagramResult{
		User:  userID,
		ReqID: reqID,
	}).Error; err != nil {
		return []byte{}, err
	}

	decData, err := base64.StdEncoding.DecodeString(igResult.Data)
	if err != nil {
		return []byte{}, err
	}

	//decompress data
	reader, err := gzip.NewReader(bytes.NewReader(decData))
	if err != nil {
		return []byte{}, err
	}

	return ioutil.ReadAll(reader)
}

func loadCSV(db *gorm.DB, reqID, userID string) ([]byte, error) {
	lnResult := new(models.InstagramResult)
	if err := db.Take(lnResult, models.InstagramResult{
		User:  userID,
		ReqID: reqID,
		Type:  "csv",
	}).Error; err != nil {
		return []byte{}, err
	}

	return base64.StdEncoding.DecodeString(lnResult.Data)
}
