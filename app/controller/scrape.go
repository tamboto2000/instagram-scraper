package controller

import (
	"collin/spider/util"
	"controller"
	"encoding/base64"
	"instagram_scraper/app/misc/reporter"
	"instagram_scraper/app/models"
	"instagram_scraper/app/worker/instagram"
	"strconv"

	"github.com/jinzhu/gorm"
)

func startScraper(ctx *controller.Context) {
	err := ctx.Request.ParseForm()
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	userID := ctx.Request.FormValue("userID")
	csvString := ctx.Request.FormValue("csv")
	lable := ctx.Request.FormValue("lable")
	accountCol := ctx.Request.FormValue("accountCol")
	uniqueIDCol := ctx.Request.FormValue("uniqueIDCol")
	postLimitStr := ctx.Request.FormValue("postLimit")
	includeComment := ctx.Request.FormValue("includeComment")

	if userID == "" || csvString == "" || lable == "" || accountCol == "" || uniqueIDCol == "" {
		ctx.BadRequest("all parameter except postLimit and includeComment is need to be filled")
		return
	}

	if includeComment != "false" && includeComment != "true" {
		ctx.BadRequest("invalid value for includeComment")
		return
	}

	postLimit := 0
	if postLimitStr != "" {
		postLimit, err = strconv.Atoi(postLimitStr)
		if err != nil {
			ctx.BadRequest("invalid value for postLimit")
			return
		}
	}

	csvMap, err := parseCSV(csvString)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	reqID := util.RandString(20)
	token := util.RandString(40)

	scraperWorker := instagram.New()
	scraperWorker.DB = ctx.App.DB
	scraperWorker.UserID = userID
	scraperWorker.CSVData = csvMap
	scraperWorker.Lable = lable
	scraperWorker.UsernameCol = accountCol
	scraperWorker.UniqueIDCol = uniqueIDCol
	scraperWorker.PostLimit = postLimit
	scraperWorker.ReqID = reqID

	if includeComment == "true" {
		scraperWorker.IncludeComment = true
	} else {
		scraperWorker.IncludeComment = false
	}

	igSession := new(models.InstagramSession)
	if err := ctx.App.DB.Take(igSession, models.InstagramSession{UserID: userID}).Error; err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			ctx.Forbidden("instagram session not found")
			return
		}

		ctx.InternalError(err.Error())
		return
	}

	scraperWorker.IgSession = igSession.Session
	err = createReporterRoom(reqID, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	scraperWorker.Reporter, err = createReporter(reqID, token)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	ctx.App.Worker.Add("instagram-"+reqID, scraperWorker)
	ctx.App.Worker.Run("instagram-" + reqID)

	ctx.WriteJSON(map[string]interface{}{
		"status":     "OK",
		"statusCode": 200,
		"requestID":  reqID,
		"token":      token,
	})
}

func createReporterRoom(reqID, pass string) error {
	request := util.NewRequest()
	// request.SetUrl("http://31.220.61.116:8000/createRoom")
	request.SetUrl("http://172.18.0.2:8000/createRoom")
	request.SetQuery("roomID", "instagram-"+reqID)
	request.SetQuery("roomName", "Instagram Report Channel")
	request.SetQuery("protected", "true")
	request.SetQuery("password", pass)
	err := request.Execute()
	defer request.Close()

	return err
}

func createReporter(reqID, pass string) (*reporter.Reporter, error) {
	reporter := reporter.NewReporter()
	reporter.ConnID = "instagram_master"
	reporter.RoomID = "instagram-" + reqID
	reporter.Password = pass
	reporter.Log = ctrl.Log

	return reporter, reporter.Connect()
}

func parseCSV(str string) ([]map[string]string, error) {
	decoded, err := base64.StdEncoding.DecodeString(str)
	if err != nil {
		return []map[string]string{}, err
	}

	return util.ParseCSVStringToMap(string(decoded))
}
