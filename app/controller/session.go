package controller

import (
	"bytes"
	"controller"
	"instagram_service/models"

	"github.com/jinzhu/gorm"

	"github.com/ahmdrz/goinsta"
)

//CreateIGSession create saved session for Instagram
func createIGSession(ctx *controller.Context) {
	err := ctx.Request.ParseForm()
	if err != nil {
		ctx.BadRequest(err.Error())
		return
	}

	userID := ctx.Request.FormValue("userID")
	username := ctx.Request.FormValue("username")
	password := ctx.Request.FormValue("password")

	if userID == "" || username == "" || password == "" {
		ctx.BadRequest("userID, username, and password need to be filled")
		return
	}

	ig := goinsta.New(username, password)

	err = ig.Login()
	if err != nil {
		ctx.Forbidden("your instagram credential is invalid")
		return
	}

	exportedCred := ""
	buff := bytes.NewBufferString(exportedCred)
	err = goinsta.Export(ig, buff)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	instagramSession := new(models.InstagramSession)
	sessNotExist := ctrl.DB.Where("user_id = ?", userID).Take(instagramSession).RecordNotFound()

	if sessNotExist {
		instagramSession := models.InstagramSession{
			UserID:  userID,
			Session: buff.String(),
		}

		if err := ctrl.DB.Create(&instagramSession).Error; err != nil {
			ctx.InternalError(err.Error())
			return
		}
	} else {
		instagramSession := models.InstagramSession{
			UserID:  userID,
			Session: buff.String(),
		}

		if err := ctrl.DB.Where("user_id = ?", userID).Update(&instagramSession).Error; err != nil {
			ctx.InternalError(err.Error())
			return
		}
	}

	ctx.RequestOK("success")
}

//CheckOldInstagramSession check the valid state of previous created Instagram session
func checkOldInstagramSession(ctx *controller.Context) {
	userID := ctx.Request.URL.Query().Get("userID")

	instagramSess := new(models.InstagramSession)
	err := ctrl.DB.Where("user_id = ?", userID).Take(instagramSess).Error
	if err != nil {
		if err.Error() == gorm.ErrRecordNotFound.Error() {
			ctx.NotFound("session not found")
			return
		}

		ctx.InternalError(err.Error())
		return
	}

	igSess := instagramSess.Session

	buff := bytes.NewBufferString(igSess)
	ig, err := goinsta.ImportReader(buff)
	if err != nil {
		ctx.InternalError(err.Error())
		return
	}

	err = ig.Account.Sync()
	if err != nil {
		ctx.RequestOK("session invalid")
		return
	}

	ctx.RequestOK("session valid")
}
