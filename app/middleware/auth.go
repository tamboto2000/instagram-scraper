package middleware

import (
	"fmt"
	"net/http"
)

//Auth is an middleware example
func Auth(w http.ResponseWriter, r *http.Request) bool {
	fmt.Println("Hello middleware")

	//Deny request
	// w.Write([]byte("denied"))
	// return false

	return true
}
