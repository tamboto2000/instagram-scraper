package main

import (
	"instagram_scraper/app/models"

	"github.com/jinzhu/gorm"
)

func migrate(db *gorm.DB) {
	err := db.AutoMigrate(
		models.InstagramComment{},
		models.InstagramImage{},
		models.InstagramJSON{},
		models.InstagramLocation{},
		models.InstagramPost{},
		models.InstagramResult{},
		models.InstagramSession{},
		models.InstagramVideo{},
		models.ProxyList{},
	).Error

	if err != nil {
		panic(err.Error())
	}
}

func reMigrate(db *gorm.DB) {
	db.DropTableIfExists(
		models.InstagramComment{},
		models.InstagramImage{},
		models.InstagramJSON{},
		models.InstagramLocation{},
		models.InstagramPost{},
		models.InstagramResult{},
		models.InstagramSession{},
		models.InstagramVideo{},
		models.ProxyList{},
	)

	db.CreateTable(
		models.InstagramComment{},
		models.InstagramImage{},
		models.InstagramJSON{},
		models.InstagramLocation{},
		models.InstagramPost{},
		models.InstagramResult{},
		models.InstagramSession{},
		models.InstagramVideo{},
		models.ProxyList{},
	)
}
