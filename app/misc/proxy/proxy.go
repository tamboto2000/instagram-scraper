package proxy

import (
	"instagram_scraper/app/models"

	"github.com/jinzhu/gorm"
)

//GetProxies fetch proxy, fetch on amount you need.
func GetProxies(db *gorm.DB, need int, opt []string) ([]models.ProxyList, error) {
	return getProxies(db, need, opt)
}

//GetProxy fetch one proxy defined by country code
func GetProxy(db *gorm.DB, code string) (string, error) {
	proxy := new(models.ProxyList)
	if err := db.Table("proxy_lists").Where(models.ProxyList{Code: code}).Take(proxy).Error; err != nil {
		return "", err
	}

	return proxy.IPAndPort, nil
}

//DeleteProxy delete a proxy specified by url (example: http://127.76.34.4:8364)
func DeleteProxy(db *gorm.DB, url string) error {
	return db.Table("proxy_lists").Where(models.ProxyList{IPAndPort: url}).Delete(models.ProxyList{}).Error
}

func getProxyCount(db *gorm.DB, code string) (int, error) {
	count := new(int)
	if err := db.Table("proxy_lists").Table("proxy_lists").Where(models.ProxyList{Code: code}).Count(count).Error; err != nil {
		return *count, err
	}

	return *count, nil
}

func getProxy(db *gorm.DB, code string) (*models.ProxyList, error) {
	proxy := new(models.ProxyList)
	if err := db.Table("proxy_lists").Where(models.ProxyList{Code: code}).Take(proxy).Error; err != nil {
		return nil, err
	}

	return proxy, nil
}

func getProxies(db *gorm.DB, need int, opt []string) ([]models.ProxyList, error) {
	proxies := []models.ProxyList{}

	for _, code := range opt {
		proxCount, err := getProxyCount(db, code)
		if err != nil {
			if err.Error() == "not found" {
				continue
			}

			return proxies, err
		}

		count := 0
		if need > proxCount {
			count = proxCount
		} else {
			count = need
		}

		for i := 0; i < count; i++ {
			proxy, err := getProxy(db, code)
			if err != nil {
				if err.Error() == "not found" {
					continue
				}

				return proxies, err
			}

			proxies = append(proxies, *proxy)
		}

		if need > proxCount {
			continue
		}

		break
	}

	return proxies, nil
}
