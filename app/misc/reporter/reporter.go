package reporter

import (
	"errors"
	"io/ioutil"
	"net/url"
	"sync"

	"log"

	"github.com/gorilla/websocket"
)

type Message struct {
	ID      string   `json:"id"`
	From    string   `json:"from"`
	Type    string   `json:"type"`
	Target  string   `json:"target,omitempty"`
	Targets []string `json:"targsets,omitempty"`
	Message string   `json:"message"`
}

type Reporter struct {
	ConnID   string
	RoomID   string
	Password string
	Log      *log.Log
	Error    error
	conn     *websocket.Conn
	mx       *sync.Mutex
}

func NewReporter() *Reporter {
	return &Reporter{
		mx: new(sync.Mutex),
	}
}

const wsURL = "WS_REPORTER_URL"

func (r *Reporter) Connect() error {
	query := url.Values{}
	query.Add("roomID", r.RoomID)
	query.Add("password", r.Password)
	query.Add("connID", r.ConnID)

	// u := url.URL{Scheme: "ws", Host: "31.220.61.116:8000", Path: "/connect", RawQuery: query.Encode()}
	u := url.URL{Scheme: "ws", Host: "172.18.0.2:8000", Path: "/connect", RawQuery: query.Encode()}

	c, resp, err := websocket.DefaultDialer.Dial(u.String(), nil)
	if err != nil {
		bytes, _ := ioutil.ReadAll(resp.Body)
		return errors.New(err.Error() + ": " + string(bytes))
	}

	r.conn = c

	return nil
}

func (r *Reporter) Write(ty, msg string) {
	message := Message{
		Type:    ty,
		Message: msg,
	}

	r.mx.Lock()
	err := websocket.WriteJSON(r.conn, message)
	if err != nil {
		r.Log.Write("websocket_error_"+r.RoomID, err.Error())
	}
	r.mx.Unlock()
}

func (r *Reporter) Read() {
	for {
		message := new(Message)
		err := websocket.ReadJSON(r.conn, message)
		if err != nil {
			r.Log.Write("websocket_error_"+r.RoomID, err.Error())
		}

		r.Error = err
	}
}
