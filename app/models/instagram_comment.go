package models

import "time"

//InstagramComment is an ORM
type InstagramComment struct {
	ID            int    `gorm:"AUTO_INCREMENT"`
	ReqID         string `gorm:"size:255"`
	User          string `gorm:"size:255"`
	PostID        string `gorm:"size:255"`
	CommentID     string `gorm:"size:255"`
	Text          string `gorm:"type:longtext"`
	LikeCount     int
	Type          int
	ContentType   string
	UnixTimestamp int64
	CreatedAt     time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt     time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
