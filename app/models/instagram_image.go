package models

import "time"

//InstagramImage is an ORM
type InstagramImage struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	ReqID     string `gorm:"size:255"`
	User      string `gorm:"size:255"`
	PostID    string `gorm:"size:255"`
	Width     int
	Height    int
	URL       string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
