package models

import "time"

//InstagramJSON is an ORM for instagram_json
type InstagramJSON struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	UserID    string    `gorm:"column:user_id"`
	ReqID     string    `gorm:"column:req_id"`
	Lable     string    `gorm:"column:lable"`
	DataType  string    `gorm:"column:data_type"`
	DataKey   string    `gorm:"column:data_key"`
	Data      string    `gorm:"column:data"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
