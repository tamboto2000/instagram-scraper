package models

import "time"

//InstagramLocation is an ORM
type InstagramLocation struct {
	ID        int    `gorm:"AUTO_INCREMENT"`
	ReqID     string `gorm:"size:255"`
	User      string `gorm:"size:255"`
	PostID    string `gorm:"size:255"`
	Name      string `gorm:"type:text"`
	Address   string `gorm:"type:text"`
	City      string `gorm:"size:255"`
	ShortName string `gorm:"size:255"`
	Long      float64
	Lat       float64
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
