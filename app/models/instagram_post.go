package models

import "time"

//InstagramPost is an ORM
type InstagramPost struct {
	ID            int    `gorm:"AUTO_INCREMENT"`
	User          string `gorm:"size:255"`
	ReqID         string `gorm:"size:255"`
	PostID        string `gorm:"size:255"`
	Code          string `gorm:"size:255"`
	URL           string `gorm:"size:1000"`
	Caption       string `gorm:"type:longtext"`
	LikeCount     int
	CommentCount  int
	UnixTimestamp int64
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
