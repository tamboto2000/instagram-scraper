package models

import "time"

//InstagramResult is an ORM
type InstagramResult struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	User      string    `gorm:"size:255"`
	ReqID     string    `gorm:"size:255"`
	Lable     string    `gorm:"size:255"`
	Type      string    `gorm:"type:enum('json','csv')"`
	Name      string    `gorm:"size:255"`
	Data      string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
