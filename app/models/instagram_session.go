package models

import "time"

//InstagramSession is an ORM for table instagram_session
type InstagramSession struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	UserID    string    `gorm:"column:user_id"`
	Session   string    `gorm:"type:mediumtext"`
	CreatedAt time.Time `gorm:"column:created_at;not null;default:CURRENT_TIMESTAMP"`
	UpdatedAt time.Time `gorm:"column:updated_at;not null;default:CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"`
}
