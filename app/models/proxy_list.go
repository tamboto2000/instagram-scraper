package models

import "time"

//ProxyList is an ORM
type ProxyList struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	IPAndPort string    `gorm:"column:IP_AND_PORT;size:255"`
	Code      string    `gorm:"column:CODE"`
	Country   string    `gorm:"column:COUNTRY"`
	Anonimity string    `gorm:"column:ANONIMITY"`
	HTTPS     string    `gorm:"column:HTTPS"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp" gorm:"default:current_timestamp"`
}
