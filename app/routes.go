package main

import (
	"router"
)

var routes = router.NewRouter()

//Routes register routes
func Routes() *router.Router {
	routes.AddFunc("/create_session", ctrl.Controller("create_session"), "POST")
	routes.AddFunc("/check_session", ctrl.Controller("check_session"), "GET")
	routes.AddFunc("/scrape", ctrl.Controller("start_scraper"), "POST")
	routes.AddFunc("/download", ctrl.Controller("download"), "GET")

	return routes
}
