package instagram

import (
	"bytes"
	"encoding/base64"
	"encoding/csv"
	"fmt"
	"instagram_scraper/app/misc/reporter"
	"instagram_scraper/app/misc/ziper"
	"instagram_scraper/app/models"
	"strconv"
	"sync"

	"github.com/ahmdrz/goinsta"
	"github.com/jinzhu/gorm"
	"github.com/mohae/struct2csv"
)

type indexToCSVWorker struct {
	db             *gorm.DB
	userID         string
	reqID          string
	uniqueIDCol    string
	usernameCol    string
	lable          string
	postLimit      int
	includeComment bool
	csvData        []map[string]string
	igSession      string
	client         *goinsta.Instagram
	result         *Result
	reporter       *reporter.Reporter
	csvResultMap   *sync.Map
	log            chan [2]string
	wg             *sync.WaitGroup
}

func (wk *indexToCSVWorker) run() {
	wk.wg.Add(5)
	go wk.indexPostData()
	go wk.indexComment()
	go wk.indexImage()
	go wk.indexVideo()
	go wk.indexLocation()
	wk.wg.Wait()

	csvBytes, err := wk.compressCSVToZIP()
	if err != nil {
		wk.log <- [2]string{"instagram_scraper_" + wk.reqID, err.Error()}
		wk.reporter.Write("fatal_error", err.Error())
	}

	encoded := base64.StdEncoding.EncodeToString(csvBytes)
	if err := wk.db.Create(&models.InstagramResult{
		User:  wk.userID,
		ReqID: wk.reqID,
		Lable: wk.lable,
		Type:  "csv",
		Name:  "result.zip",
		Data:  encoded,
	}).Error; err != nil {
		if err != nil {
			wk.log <- [2]string{"instagram_scraper_" + wk.reqID, err.Error()}
			wk.reporter.Write("fatal_error", err.Error())
		}
	}
}

func (wk *indexToCSVWorker) indexPostData() {
	igPosts := []models.InstagramPost{}

	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		fmt.Println(key.(string), "(indexPostData)")
		igPost := val.(*IGFeed).Feed
		for _, post := range igPost {
			igPosts = append(igPosts, models.InstagramPost{
				User:          key.(string),
				ReqID:         wk.reqID,
				PostID:        post.ID,
				Code:          post.Code,
				URL:           "https://www.instagram.com/p/" + post.Code,
				Caption:       post.Caption.Text,
				LikeCount:     post.Likes,
				CommentCount:  post.CommentCount,
				UnixTimestamp: post.TakenAt,
			})
		}

		return true
	})

	fmt.Println(igPosts)
	wk.csvResultMap.Store("posts.csv", igPosts)
	fmt.Println("file posts.csv created")

	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexComment() {
	igComments := []models.InstagramComment{}

	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		fmt.Println(key.(string), "(indexComment)")
		igComment := val.(*IGFeed).PostComment
		for postID, comments := range igComment {
			for _, comment := range comments {
				igComments = append(igComments, models.InstagramComment{
					User:          key.(string),
					ReqID:         wk.reqID,
					PostID:        postID,
					CommentID:     strconv.FormatInt(comment.ID, 10),
					Text:          comment.Text,
					LikeCount:     comment.CommentLikeCount,
					Type:          comment.Type,
					ContentType:   comment.ContentType,
					UnixTimestamp: comment.CreatedAt,
				})
			}
		}

		return true
	})

	wk.csvResultMap.Store("comments.csv", igComments)
	fmt.Println("file comments.csv created")

	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexImage() {
	igImages := []models.InstagramImage{}

	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		igPosts := val.(*IGFeed).Feed
		for _, post := range igPosts {
			if len(post.Images.Versions) > 0 {
				best := post.Images.Versions[0]
				igImages = append(igImages, models.InstagramImage{
					User:   key.(string),
					ReqID:  wk.reqID,
					PostID: post.ID,
					Width:  best.Width,
					Height: best.Height,
					URL:    best.URL,
				})
			}
		}

		return true
	})

	wk.csvResultMap.Store("images.csv", igImages)
	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexVideo() {
	igVideos := []models.InstagramVideo{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		igPosts := val.(*IGFeed).Feed
		for _, post := range igPosts {
			if len(post.Videos) > 0 {
				best := post.Videos[0]

				igVideos = append(igVideos, models.InstagramVideo{
					User:    key.(string),
					ReqID:   wk.reqID,
					PostID:  post.ID,
					VideoID: best.ID,
					Width:   best.Width,
					Height:  best.Height,
					URL:     best.URL,
				})
			}
		}

		return true
	})

	wk.csvResultMap.Store("videos.csv", igVideos)
	wk.wg.Done()
}

func (wk *indexToCSVWorker) indexLocation() {
	igLocations := []models.InstagramLocation{}
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		igPosts := val.(*IGFeed).Feed
		for _, post := range igPosts {
			if post.Location.Lat != 0 && post.Location.Lng != 0 {
				location := post.Location
				igLocations = append(igLocations, models.InstagramLocation{
					User:      key.(string),
					ReqID:     wk.reqID,
					PostID:    post.ID,
					Name:      location.Name,
					Address:   location.Address,
					City:      location.City,
					ShortName: location.ShortName,
					Long:      location.Lng,
					Lat:       location.Lat,
				})
			}
		}

		return true
	})

	wk.wg.Done()
}

func (wk *indexToCSVWorker) compressCSVToZIP() ([]byte, error) {
	zipWriter := ziper.NewZiper()

	wk.csvResultMap.Range(func(key, val interface{}) bool {
		//Convert to csv array
		structToCSV := struct2csv.New()
		csvArray, err := structToCSV.Marshal(val)
		if err != nil {
			fmt.Println(err.Error())
			return true
		}

		//Convert arrays to csv string
		str := ""
		buff := bytes.NewBufferString(str)
		writer := csv.NewWriter(buff)
		err = writer.WriteAll(csvArray)
		if err != nil {
			zipWriter.Close()
			fmt.Println(err.Error())
			return true
		}

		zipWriter.Add(key.(string), buff.String())

		return true
	})

	err := zipWriter.Close()
	if err != nil {
		return []byte{}, err
	}

	return zipWriter.Bytes(), nil
}
