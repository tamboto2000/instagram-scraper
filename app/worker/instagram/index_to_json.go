package instagram

import (
	"bytes"
	"compress/gzip"
	"encoding/base64"
	"encoding/json"
	"instagram_scraper/app/misc/reporter"
	"instagram_scraper/app/models"
	"sync"

	"github.com/jinzhu/gorm"

	"github.com/ahmdrz/goinsta"
)

type indexToJSONWorker struct {
	db             *gorm.DB
	userID         string
	reqID          string
	uniqueIDCol    string
	usernameCol    string
	lable          string
	postLimit      int
	includeComment bool
	csvData        []map[string]string
	igSession      string
	client         *goinsta.Instagram
	result         *Result
	reporter       *reporter.Reporter
	log            chan [2]string
	wg             *sync.WaitGroup
}

func (wk *indexToJSONWorker) run() {
	wk.storeToDB()
}

func (wk *indexToJSONWorker) storeToDB() {
	jsonBytes, err := wk.normalization()
	if err != nil {
		go wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"instagram_scraper_" + wk.reqID, err.Error()}
	}

	compressed, err := compressAndEncode(jsonBytes)
	if err != nil {
		go wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"instagram_scraper_" + wk.reqID, err.Error()}
	}

	if err := wk.db.Create(&models.InstagramResult{
		User:  wk.userID,
		ReqID: wk.reqID,
		Lable: wk.lable,
		Type:  "json",
		Name:  "result.json",
		Data:  compressed,
	}).Error; err != nil {
		go wk.reporter.Write("fatal_error", err.Error())
		wk.log <- [2]string{"instagram_scraper_" + wk.reqID, err.Error()}
	}
}

func (wk *indexToJSONWorker) normalization() ([]byte, error) {
	wk.result.ResultMap.Range(func(key, val interface{}) bool {
		wk.result.Result[key.(string)] = val.(*IGFeed)

		return true
	})

	return json.Marshal(wk.result)
}

func compressAndEncode(data []byte) (string, error) {
	buffer := bytes.NewBuffer([]byte{})
	gzWriter, err := gzip.NewWriterLevel(buffer, gzip.BestCompression)
	if err != nil {
		return "", err
	}

	if _, err := gzWriter.Write(data); err != nil {
		return "", err
	}

	if err := gzWriter.Close(); err != nil {
		return "", err
	}

	enc := base64.StdEncoding.EncodeToString(buffer.Bytes())
	return enc, nil
}
