package instagram

import (
	"instagram_scraper/app/misc/reporter"
	"sync"

	"github.com/jinzhu/gorm"

	"github.com/ahmdrz/goinsta"
)

type InstagramScraper struct {
	DB                *gorm.DB
	UserID            string
	ReqID             string
	UniqueIDCol       string
	UsernameCol       string
	Lable             string
	PostLimit         int
	IncludeComment    bool
	CSVData           []map[string]string
	IgSession         string
	Client            *goinsta.Instagram
	Result            Result
	Reporter          *reporter.Reporter
	log               chan [2]string
	wg                *sync.WaitGroup
	scrapeWorker      *scrapeWorker
	indexToJSONWorker *indexToJSONWorker
	exit              chan bool
}

func New() *InstagramScraper {
	return &InstagramScraper{
		log:  make(chan [2]string),
		wg:   new(sync.WaitGroup),
		exit: make(chan bool),
	}
}

func (wk InstagramScraper) Run() error {
	go func() {
		wk.runScrapeWorker()
		wk.scrapeWorker.result.ResultMap.Range(func(key, val interface{}) bool {
			return true
		})
		wk.runIndexToJSONWorker()
		wk.runIndexToCSVWorker()
		wk.Reporter.Write("finish", "finish")
		wk.exit <- true
	}()

	return nil
}

func (wk *InstagramScraper) runScrapeWorker() {
	worker := &scrapeWorker{
		db:             wk.DB,
		userID:         wk.UserID,
		reqID:          wk.ReqID,
		uniqueIDCol:    wk.UniqueIDCol,
		usernameCol:    wk.UsernameCol,
		lable:          wk.Lable,
		postLimit:      wk.PostLimit,
		includeComment: wk.IncludeComment,
		csvData:        wk.CSVData,
		igSession:      wk.IgSession,
		client:         wk.Client,
		result:         wk.Result,
		reporter:       wk.Reporter,
		log:            wk.log,
		wg:             new(sync.WaitGroup),
	}

	worker.result.Result = make(map[string]*IGFeed)
	worker.result.ResultMap = new(sync.Map)

	wk.scrapeWorker = worker
	worker.run()
}

func (wk *InstagramScraper) runIndexToJSONWorker() {
	worker := &indexToJSONWorker{
		db:             wk.DB,
		userID:         wk.UserID,
		reqID:          wk.ReqID,
		uniqueIDCol:    wk.UniqueIDCol,
		usernameCol:    wk.UsernameCol,
		lable:          wk.Lable,
		postLimit:      wk.PostLimit,
		includeComment: wk.IncludeComment,
		csvData:        wk.CSVData,
		igSession:      wk.IgSession,
		client:         wk.Client,
		result:         &wk.scrapeWorker.result,
		reporter:       wk.Reporter,
		log:            wk.log,
		wg:             new(sync.WaitGroup),
	}

	wk.indexToJSONWorker = worker
	worker.run()
}

func (wk *InstagramScraper) runIndexToCSVWorker() {
	worker := &indexToCSVWorker{
		db:             wk.DB,
		userID:         wk.UserID,
		reqID:          wk.ReqID,
		uniqueIDCol:    wk.UniqueIDCol,
		usernameCol:    wk.UsernameCol,
		lable:          wk.Lable,
		postLimit:      wk.PostLimit,
		includeComment: wk.IncludeComment,
		csvData:        wk.CSVData,
		igSession:      wk.IgSession,
		client:         wk.Client,
		result:         &wk.scrapeWorker.result,
		reporter:       wk.Reporter,
		csvResultMap:   new(sync.Map),
		log:            wk.log,
		wg:             new(sync.WaitGroup),
	}

	worker.run()
}

func (wk *InstagramScraper) Wait() {
	<-wk.exit
}

func (wk *InstagramScraper) ReadLog() chan [2]string {
	return wk.log
}

func (wk *InstagramScraper) TerminateOnFinish() bool {
	return true
}

func (wk *InstagramScraper) CleanUp() error {
	return nil
}
