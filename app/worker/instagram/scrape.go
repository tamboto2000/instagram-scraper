package instagram

import (
	"bytes"
	"fmt"
	"instagram_scraper/app/misc/proxy"
	"instagram_service/misc"
	"net/http"
	"time"

	"instagram_scraper/app/misc/reporter"
	"strings"
	"sync"

	"github.com/ahmdrz/goinsta"
	"github.com/jinzhu/gorm"
)

//Result is the result from scraping
type Result struct {
	Fail         []Fail             `json:"fail,omitempty"`
	Success      []string           `json:"success,omitempty"`
	ResultMap    *sync.Map          `json:"-"`
	Result       map[string]*IGFeed `json:"result"`
	Empty        []string           `json:"empty"`
	PostCount    int                `json:"postCount"`
	FailCount    int                `json:"failCount"`
	SuccessCount int                `json:"successCount"`
	EmptyCount   int                `json:"emptyCount"`
}

type IGFeed struct {
	Feed        map[string]goinsta.Item      `json:"feed"`
	PostComment map[string][]goinsta.Comment `json:"postComment"`
}

type Success struct {
	UserID          string `json:"UserID"`
	TwitterUsername string `json:"twitterUsername"`
}

type Fail struct {
	Type              string `json:"type"`
	UserID            string `json:"UserID,omitempty"`
	PostID            string `json:"postID,omitempty"`
	InstagramUsername string `json:"instagramUsername,omitempty"`
	ErrorMessage      string `json:"errorMessage"`
}

const (
	errAccountInvalid  = "instagram_username_invalid"
	errAccountNotFound = "instagram_account_not_found"
)

type scrapeWorker struct {
	db             *gorm.DB
	userID         string
	reqID          string
	uniqueIDCol    string
	usernameCol    string
	lable          string
	postLimit      int
	includeComment bool
	csvData        []map[string]string
	igSession      string
	client         *goinsta.Instagram
	result         Result
	reporter       *reporter.Reporter
	log            chan [2]string
	wg             *sync.WaitGroup
}

func (wk *scrapeWorker) createClient() error {
	buff := bytes.NewReader([]byte(wk.igSession))
	ig, err := goinsta.ImportReader(buff)
	if err != nil {
		return err
	}

	wk.client = ig

	return nil
}

func (wk *scrapeWorker) createClientWithProxy(ip string) *goinsta.Instagram {
	igCl := *wk.client
	customCl := http.Client{
		Timeout: 10 * time.Second,
	}

	igCl.SetHTTPClient(&customCl)

	(&igCl).SetProxy(ip, true)

	return &igCl
}

func (wk *scrapeWorker) run() {
	wk.reporter.Write("info", "starting scraper...")
	wk.divideAndRun()
	wk.reporter.Write("info", "scraper started")
	wk.wg.Wait()
	wk.reporter.Write("info", "Scraper finish, start indexing...")
}

//run multiple scrape function in concurrent
func (wk *scrapeWorker) divideAndRun() {
	err := wk.createClient()
	if err != nil {
		wk.reporter.Write("fatal_error", err.Error())
	}

	accCount := len(wk.csvData)
	if accCount <= 20 {
		proxies, err := proxy.GetProxies(wk.db, 1, []string{"ID", "SG", "TH"})
		ig := new(goinsta.Instagram)
		if err == nil && len(proxies) > 0 {
			ig = wk.createClientWithProxy(proxies[0].IPAndPort)
		} else {
			ig = wk.client
			if err != nil {
				go wk.reporter.Write("fatal_error", err.Error())
				go wk.reporter.Write("exit_1", "no longer can continue the scraping proccess")
				return
			}
		}

		wk.wg.Add(1)
		go wk.scrape(ig, wk.csvData)

		return
	}

	div := accCount / 20
	proxies, proxErr := misc.GetProxies(div+1, []string{"ID", "SG", "TH"})
	counter := 20
	divCounter := 0
	users := []map[string]string{}

	for _, row := range wk.csvData {
		if filterUsername(row[wk.usernameCol]) == "" {
			go wk.reporter.Write("error", "User "+row[wk.uniqueIDCol]+" has invalid instagram username string")
			continue
		}

		users := append(users, row)
		counter--

		if counter == 0 {
			ig := new(goinsta.Instagram)
			if proxErr == nil {
				ig = wk.createClientWithProxy(proxies[divCounter].IPAndPort)
			} else {
				ig = wk.client
			}

			wk.wg.Add(1)
			go wk.scrape(ig, users)
			users = []map[string]string{}
			counter = 20
			divCounter++
		}
	}

	if len(users) > 0 {
		ig := new(goinsta.Instagram)

		if proxErr == nil {
			ig = wk.createClientWithProxy(proxies[divCounter].IPAndPort)
		} else {
			ig = wk.client
		}

		wk.wg.Add(1)
		go wk.scrape(ig, users)
	}
}

func (wk *scrapeWorker) scrape(ig *goinsta.Instagram, users []map[string]string) {
	for _, row := range users {
		username := row[wk.usernameCol]
		uniqueID := row[wk.uniqueIDCol]
		user, err := ig.Profiles.ByName(username)
		wk.reporter.Write("info", "Scraping user "+uniqueID)

		if err != nil {
			if strings.Contains(err.Error(), "connect: connection timed out") {
				ig.UnsetProxy()
				user, err := ig.Profiles.ByName(username)

				igFeed, err := wk.getFeeds(user, uniqueID, username)
				if err != nil {
					wk.reporter.Write("error", "Error while scraping user "+uniqueID+": "+err.Error())
				}

				wk.result.ResultMap.Store(uniqueID, igFeed)
				wk.reporter.Write("info", "User "+uniqueID+" is scraped")

				continue
			}

			go wk.reporter.Write("error", "User "+uniqueID+": "+err.Error())

			wk.result.Fail = append(wk.result.Fail, Fail{
				Type:         errAccountNotFound,
				UserID:       uniqueID,
				ErrorMessage: err.Error(),
			})

			continue
		}

		igFeed, err := wk.getFeeds(user, uniqueID, username)
		if err != nil {
			wk.reporter.Write("error", "Error while scraping user "+uniqueID+": "+err.Error())
		}

		wk.result.ResultMap.Store(uniqueID, igFeed)
		wk.reporter.Write("info", "User "+uniqueID+" is scraped")
	}

	wk.wg.Done()
}

func (wk *scrapeWorker) getFeeds(user *goinsta.User, userID, username string) (*IGFeed, error) {
	fmt.Println("get feeds")
	igFeed := new(IGFeed)

	media := user.Feed()
	err := media.Error()
	if err != nil {
		wk.result.Fail = append(wk.result.Fail, Fail{
			Type:              "internal_error",
			UserID:            userID,
			InstagramUsername: username,
			ErrorMessage:      err.Error(),
		})

		return nil, err
	}

	feeds := make(map[string]goinsta.Item)
	comments := make(map[string][]goinsta.Comment)
	counter := 0

	for media.Next() {
		for _, item := range media.Items {
			feeds[item.ID] = item
			if wk.includeComment {
				comms := getComment(&item)
				comments[item.ID] = comms
			}

			counter++
			if wk.postLimit > 0 && counter == wk.postLimit {
				break
			}
		}

		if wk.postLimit > 0 && counter == wk.postLimit {
			break
		}
	}

	igFeed.Feed = feeds
	igFeed.PostComment = comments

	return igFeed, nil
}

func getComment(item *goinsta.Item) []goinsta.Comment {
	comments := []goinsta.Comment{}
	item.Comments.Sync()
	for item.Comments.Next() {
		comments = append(comments, item.Comments.Items...)
	}

	return comments
}

func filterUsername(user string) string {
	if strings.Contains(user, " ") {
		return ""
	}

	if strings.Contains(user, "@") {
		user = strings.Replace(user, "@", "", -1)
		return user
	}

	if strings.Contains(user, "/") {
		stg1 := strings.Split(user, "/")
		if stg1[len(stg1)-1] != "" {
			return stg1[len(stg1)-1]
		} else {
			if strings.Contains(stg1[len(stg1)-2], "gram.com") {
				return ""
			} else {
				return stg1[len(stg1)-2]
			}
		}
	}

	return user
}
