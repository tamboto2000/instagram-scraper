package worker

import (
	"worker"
)

var pool = worker.NewPool()

//Pool return pool for workers
func Pool() *worker.Pool {
	return pool
}
