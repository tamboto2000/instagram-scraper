package controller

import (
	"errors"
	"log"
	"net/http"
	"worker"

	"github.com/jinzhu/gorm"
)

type Controller struct {
	DB     *gorm.DB
	Worker *worker.Pool
	Log    *log.Log
	funcs  map[string]HTTPFunc
}

//Response represent http response structure
type Response struct {
	Status     string `json:"status"`
	StatusCode int    `json:"statusCode"`
	Message    string `json:"message"`
	RequestID  string `json:"requestId,omitempty"`
	Token      string `json:"token,omitempty"`
}

type ctrlFunc func(ctx *Context)
type HTTPFunc func(w http.ResponseWriter, r *http.Request)

func PanicOnError(err error) {
	panic(err.Error())
}

func NewController() *Controller {
	return &Controller{
		funcs: make(map[string]HTTPFunc),
	}
}

func (ctrl *Controller) Add(name string, ctrlFunc ctrlFunc) error {
	_, exist := ctrl.funcs[name]
	if exist {
		return errors.New("controller already exist")
	}

	ctrl.funcs[name] = func(w http.ResponseWriter, r *http.Request) {
		ctx := &Context{
			App:     ctrl,
			Writer:  w,
			Request: r,
		}

		ctrlFunc(ctx)
	}

	return nil
}

func (ctrl *Controller) Controller(name string) HTTPFunc {
	ctrlFunc, exist := ctrl.funcs[name]
	if !exist {
		panic("controller doesn't exist")
	}

	return ctrlFunc
}
