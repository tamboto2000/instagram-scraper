package log

import (
	"fmt"
	"sync"
	"time"

	"github.com/jinzhu/gorm"
)

type LogReport struct {
	ID        int       `gorm:"AUTO_INCREMENT"`
	Tag       string    `gorm:"size:255"`
	Text      string    `gorm:"type:longtext"`
	CreatedAt time.Time `gorm:"column:created_at;type:timestamp" gorm:"default:current_timestamp"`
}

//Log is an singleton for logging system
type Log struct {
	db    *gorm.DB
	mutex *sync.Mutex
}

//NewLog init logging system
func NewLog(db *gorm.DB) *Log {
	if err := db.AutoMigrate(LogReport{}).Error; err != nil {
		panic(err.Error())
	}

	return &Log{
		db:    db,
		mutex: new(sync.Mutex),
	}
}

//Delete delete log with tag = ? and logID = ?
func (log *Log) Delete(tag string) {
	if err := log.db.Delete(&LogReport{
		Tag: tag,
	}).Error; err != nil {
		fmt.Println(err.Error())
	}
}

//Write write log to database
func (log *Log) Write(tag, text string) {
	log.mutex.Lock()

	logs := &LogReport{
		Text: text,
		Tag:  tag,
	}

	err := log.db.Create(logs).Error
	if err != nil {
		fmt.Println(err.Error())
	}

	log.mutex.Unlock()
}

//GetAll get all logs
func (log *Log) GetAll() []LogReport {
	logs := []LogReport{}
	err := log.db.Find(&logs).Error
	if err != nil {
		fmt.Println(err.Error())
	}

	return logs
}

//Get fetch log with optional parameter tag = ? and logID = ?
func (log *Log) Get(tag string) []LogReport {
	logs := []LogReport{}

	whereQuery := &LogReport{
		Tag: tag,
	}

	err := log.db.Table("log_tables").Where(whereQuery).Find(&logs).Error
	if err != nil {
		fmt.Println(err.Error())
	}

	return logs
}
